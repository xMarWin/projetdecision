from app import db, app
from models import Sujet,Message,Sondage,Reponse


@app.cli.command()
def initdb():
   db.create_all()

@app.cli.command()
def test():

    s1= Sujet(titre="Soirée", description="infos soirée", auteur="RORO")
    s2= Sujet(titre="Projet", description="Projet bière", auteur="Looloo")

    db.session.add(s1)
    db.session.add(s2)
    db.session.commit()


    m1=Message(contenu="Salut", auteur="RORO")
    m2=Message(contenu="cv", auteur="MichMich")
    m3=Message(contenu="ui", auteur="RORO")

    m4=Message(contenu="vous en pensez quoi ?", auteur="Looloo")
    m5=Message(contenu="jsp", auteur="RORO")
    m6=Message(contenu="fais un sondage", auteur="mattt")

    s1.messages.append(m1)
    s1.messages.append(m2)
    s1.messages.append(m3)

    s2.messages.append(m4)
    s2.messages.append(m5)
    s2.messages.append(m6)

    db.session.add(s1)
    db.session.add(s2)
    db.session.commit()

    sond1 = Sondage(question="On va à la piscine ?")
    sond2 = Sondage(question="On fini le projet ?")

    s1.sondages.append(sond1)
    db.session.add(s1)
    s2.sondages.append(sond2)
    db.session.add(s2)

    db.session.commit()

    r1 = Reponse(auteur="RORO",reponse=True)
    r2 = Reponse(auteur="MichMich",reponse=True)
    r3 = Reponse(auteur="Jessy",reponse=False)

    r4 = Reponse(auteur="Looloo",reponse=False)
    r5 = Reponse(auteur="RORO",reponse=False)
    r6 = Reponse(auteur="mattt",reponse=False)

    sond1.reponses.append(r1)
    sond1.reponses.append(r2)
    sond1.reponses.append(r3)

    sond2.reponses.append(r4)
    sond2.reponses.append(r5)
    sond2.reponses.append(r6)

    db.session.add(sond1)
    db.session.add(sond2)

    db.session.commit()

